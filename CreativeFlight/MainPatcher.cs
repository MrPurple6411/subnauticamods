﻿using Harmony;
using System.Reflection;

namespace CreativeFlight
{
    public class MainPatcher {
        public static void Patch()
        {
            var harmony = HarmonyInstance.Create("com.oldark.subnautica.creativeflight.mod");
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }

    }
}
