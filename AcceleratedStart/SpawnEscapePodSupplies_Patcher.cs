﻿using System;
using System.Collections.Generic;
using System.Linq;
using Harmony;
using System.Text;

namespace AcceleratedStart
{
    [HarmonyPatch(typeof(SpawnEscapePodSupplies))]
    [HarmonyPatch("OnNewBorn")]
    internal class SpawnEscapePodSupplies_patch
    {
        [HarmonyPrefix]
        public static bool Prefix(SpawnEscapePodSupplies __instance)
        {
            __instance.storageContainer.container.Resize(6, 10);
            return true;
        }
    }
}
