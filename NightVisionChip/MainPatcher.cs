﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using Harmony;
using UnityEngine;
using SMLHelper.V2.Handlers;
using SMLHelper.V2.Options;

namespace MAC.NightVisionChip
{
    public class MainPatcher
    {
        public static float ambientIntensity;
        public static Color ambientLight;

        public static void Patch()
        {
            NightVisionChip.PatchSMLHelper();
            var harmony = HarmonyInstance.Create("com.oldark.subnautica.nightvisionchip.mod");
            harmony.PatchAll(Assembly.GetExecutingAssembly());
            OptionsPanelHandler.RegisterModOptions(new Options("Nightvision Chip"));
        }
    }

    public class Options : ModOptions {
        public Options(string name) : base(name)
        {
            try
            {
                KeybindChanged += OnKeyBindChanged;
            }
            catch (Exception e)
            {

            }
        }

        public override void BuildModOptions()
        {
            initializeConfig();

            try
            {
                AddKeybindOption("nightvision_toggle_key", "Toggle Nightvision", GameInput.Device.Keyboard, NightVisionChipConfig.nightvision_toggle_key);
            }
            catch (Exception e)
            {

            }
        }

        public void OnKeyBindChanged(object sender, KeybindChangedEventArgs e)
        {
            switch (e.Id)
            {
                case "nightvision_toggle_key":
                    NightVisionChipConfig.nightvision_toggle_key = e.Key;
                    UpdatePlayerPrefs();
                    break;
            }
        }

        void UpdatePlayerPrefs()
        {
            int intKey = (int)NightVisionChipConfig.nightvision_toggle_key;
            PlayerPrefs.SetInt("nightvisionchip_toggle_key", intKey);
            PlayerPrefs.Save();
        }

        void initializeConfig()
        {
            if (PlayerPrefs.HasKey("nightvisionchip_toggle_key"))
            {
                NightVisionChipConfig.nightvision_toggle_key = (KeyCode)PlayerPrefs.GetInt("nightvisionchip_toggle_key");
            }
        }
    }

    public class NightVisionChipConfig : MonoBehaviour {
        public static KeyCode nightvision_toggle_key = KeyCode.N;
        public static bool nightVisionEnabled = false;
    }

}
