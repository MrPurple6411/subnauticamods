﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMLHelper.V2.Crafting;
using SMLHelper.V2.Handlers;
using SMLHelper.V2.Assets;
using UnityEngine;

namespace RedBaron {
    public class SeamothLifeSupportModule : Craftable {

        private static readonly SeamothLifeSupportModule main = new SeamothLifeSupportModule();

        internal static TechType TechTypeID { get; private set; }

        public SeamothLifeSupportModule() : base("SeamothLifeSupportModule", "Integrated Life Support", "Installs extended life support functionality, allowing the Seamoth systems to distribute water and calories to the pilot.")
        {
            OnFinishedPatching += AdditionalPatching;
        }

        public override CraftTree.Type FabricatorType { get; } = CraftTree.Type.SeamothUpgrades;
        public override TechGroup GroupForPDA { get; } = TechGroup.VehicleUpgrades;
        public override TechCategory CategoryForPDA { get; } = TechCategory.VehicleUpgrades;
        public override string AssetsFolder { get; } = "RedBaron/Assets";
        public override string[] StepsToFabricatorTab { get; } = new[] { "SeamothModules" };
        public override TechType RequiredForUnlock { get; } = TechType.None;

        public override GameObject GetGameObject()
        {
            GameObject prefab = CraftData.GetPrefabForTechType(TechType.SeamothSonarModule);
            return GameObject.Instantiate(prefab);

        }

        protected override TechData GetBlueprintRecipe()
        {
            return new TechData
            {
                craftAmount = 1,
                Ingredients =
                {
                    new Ingredient(TechType.UraniniteCrystal, 3),
                    new Ingredient(TechType.AdvancedWiringKit, 1),
                    new Ingredient(TechType.Bladderfish, 1),
                    new Ingredient(TechType.MelonSeed, 1)
                }
            };
        }

        public static void PatchSMLHelper()
        {
            main.Patch();
        }

        private void AdditionalPatching()
        {
            TechTypeID = this.TechType;
            CraftDataHandler.SetEquipmentType(this.TechType, EquipmentType.SeamothModule);
        }
    }
}
