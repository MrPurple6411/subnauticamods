﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Harmony;
using System.Reflection;
using MoreCyclopsUpgrades.API;
using MoreCyclopsUpgrades.API.General;
using UnityEngine;

namespace RedBaron {

    [HarmonyPatch(typeof(SubControl))]
    [HarmonyPatch("FixedUpdate")]
    public class SubControl_FixedUpdate_Patch {
        
        [HarmonyPrefix]
        static void PreFix(SubControl __instance)
        {


            SubRoot sub = __instance.GetComponent<SubRoot>();

            if(sub == null)
            {
                Console.WriteLine("ERROR: RedBaron: no Subroot attached to SubControl");
                return;
            }
            else if(sub.isCyclops)
            {
                FlightHandler fh = MCUServices.Find.CyclopsUpgradeHandler<FlightHandler>(sub, CyclopsFlightModule.TechTypeID);

                if(fh == null)
                {
                    return;
                }

                if (sub.powerRelay.GetPower() <= 0f)
                {
                    sub.worldForces.aboveWaterGravity = MainPatcher.originalGrav;
                    sub.worldForces.aboveWaterDrag = 0f;
                    return;
                }
                if (Ocean.main.GetDepthOf(__instance.gameObject) > 0f)
                {
                    SubRoot playerSub = Player.main.GetCurrentSub();
                    if (playerSub != null && playerSub.isCyclops)
                    {
                        if (MCUServices.CrossMod.HasUpgradeInstalled(playerSub, CyclopsFlightModule.TechTypeID) && playerSub.powerRelay.GetPower() > 0f)
                        {
                            playerSub.worldForces.aboveWaterGravity = 0f;
                            playerSub.worldForces.aboveWaterDrag = playerSub.worldForces.underwaterDrag;
                        }
                        else
                        {
                            playerSub.worldForces.aboveWaterGravity = MainPatcher.originalGrav;
                            playerSub.worldForces.aboveWaterDrag = 0f;
                        }
                    }
                }

            }

        }
    }
}