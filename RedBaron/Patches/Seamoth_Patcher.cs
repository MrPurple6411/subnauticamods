﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Harmony;

namespace RedBaron {

    [HarmonyPatch(typeof(SeaMoth))]
    [HarmonyPatch("OnUpgradeModuleChange")]
    public class Seamoth_OnUpgradeModuleChange_Patch {

        [HarmonyPostfix]
        static void PostFix(SeaMoth __instance, TechType techType)
        {

            int count = __instance.modules.GetCount(SeamothFlightModule.TechTypeID);
            if (count > 0)
            {
                __instance.worldForces.aboveWaterGravity = 0;
                __instance.moveOnLand = true;
                __instance.worldForces.aboveWaterDrag = __instance.worldForces.underwaterDrag;
            }
            else
            {
                __instance.worldForces.aboveWaterGravity = MainPatcher.originalGrav;
                __instance.worldForces.aboveWaterDrag = 0f;
                __instance.moveOnLand = false;
            }

        }
    }

    [HarmonyPatch(typeof(SeaMoth))]
    [HarmonyPatch("Update")]
    public class Seamoth_Update_Patch {
        
        [HarmonyPostfix]
        static void PostFix(SeaMoth __instance)
        {

            // Maneuvering Jets
            if(__instance.modules.GetCount(SeamothManeuveringJetsModule.TechTypeID) > 0)
            {
                float speed = 0.8f;

                if (Input.GetKey(KeyCode.Q))
                {
                    __instance.transform.Rotate(Vector3.forward, speed);
                }
                else if (Input.GetKey(KeyCode.R))
                {
                    __instance.transform.Rotate(-Vector3.forward, speed);
                }

            }

            // Life Support
            if(Player.main.inSeamoth && __instance.modules.GetCount(SeamothLifeSupportModule.TechTypeID) > 0)
            {
                if(MainPatcher.surv == null)
                {
                    MainPatcher.surv = Player.main.GetComponentInParent<Survival>();
                }

                MainPatcher.surv.food = 95f;
                MainPatcher.surv.water = 95f;

                if (!Player.main.liveMixin.IsFullHealth())
                {
                    Player.main.liveMixin.ResetHealth();
                   
                }
            }

        }
    }

    [HarmonyPatch(typeof(SeaMoth))]
    [HarmonyPatch("OnPilotModeEnd")]
    public class Seamoth_OnPilotModeEnd_Patch {

        [HarmonyPrefix]
        public static bool Prefix(SeaMoth __instance)
        {
            if (__instance.transform.position.y >= Ocean.main.GetOceanLevel())
            {
                __instance.worldForces.aboveWaterGravity = MainPatcher.originalGrav;
                __instance.moveOnLand = false;
            }

            return true;
        }
    }

    [HarmonyPatch(typeof(SeaMoth))]
    [HarmonyPatch("OnPilotModeBegin")]
    public class Seamoth_OnPilotModeBegin_Patch {

        [HarmonyPostfix]
        static void PostFix(SeaMoth __instance)
        {
            int count = __instance.modules.GetCount(SeamothFlightModule.TechTypeID);
            if (count > 0)
            {
                __instance.worldForces.aboveWaterGravity = 0;
                __instance.moveOnLand = true;
            }
            else
            {
                __instance.worldForces.aboveWaterGravity = MainPatcher.originalGrav;
                __instance.moveOnLand = false;
            }
        }
    }

}
