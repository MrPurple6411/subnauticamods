﻿using Harmony;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Unleashed_Lifepod {

    [HarmonyPatch(typeof(Player))]
    [HarmonyPatch("Update")]
    internal class Player_Update_Patch {

        public static Vector3 speed;
        [HarmonyPrefix]
        public static bool Prefix(Player __instance)
        {

            if (Input.GetKeyUp(UnleashedLifepodConfig.keyBinds["unleashedlifepod_anchor_key"]) && Vector3.Distance(__instance.transform.position, EscapePod.main.playerSpawn.position) < 5.0f)
            {
                MainPatcher.podAnchored = !MainPatcher.podAnchored;

                Beacon podData = EscapePod.main.gameObject.GetComponent<Beacon>();
                if(podData == null)
                {
                    podData = EscapePod.main.gameObject.AddComponent<Beacon>();
                    podData.beaconLabel.SetLabel("NEW BEACON!");
                }

                if (MainPatcher.podAnchored)
                {
                    ErrorMessage.AddMessage("Lifepod anchor engaged!");
                    podData.beaconLabel.SetLabel("anchored");
                    WorldForces wf = EscapePod.main.GetComponent<WorldForces>();
                    wf.underwaterGravity = 0f;
                    wf.aboveWaterGravity = 0f;
                    EscapePod.main.anchorPosition = EscapePod.main.transform.position;
                    wf.aboveWaterGravity = 9.81f;
                }
                else
                {
                    ErrorMessage.AddMessage("Lifepod anchor disengaged!");
                    podData.beaconLabel.SetLabel("not-anchored");
                    if (UnleashedLifepodConfig.heavyPod)
                    {
                        EscapePod.main.GetComponent<WorldForces>().underwaterGravity = 9.81f;
                    }
                }
            }

            if (!UnleashedLifepodConfig.allowControl)
            {
                return true;
            }
            
            if (Input.GetKey(UnleashedLifepodConfig.keyBinds["unleashedlifepod_modifier_key"]) && !MainPatcher.podAnchored)
            {
                EscapePod pod = EscapePod.main;

                if (Vector3.Distance(__instance.transform.position, EscapePod.main.playerSpawn.position) < 5.0f)
                {

                    foreach (var renderer in pod.GetComponentsInChildren<SkinnedMeshRenderer>())
                    {
                        String gameObjectName = renderer.gameObject.name;
                        Color color = renderer.material.color;
                        switch (gameObjectName)
                        {
                            // For the record, this makes it transparent from the outside
                            //case "life_pod_damaged":
                            //    renderer.material.shader = UnityEngine.Shader.Find("Transparent/Diffuse");
                            //    renderer.material.color = new Color(252, 61, 3, 1.0f;
                            //    break;
                            case "life_pod_interior_damaged1":
                                renderer.material.shader = UnityEngine.Shader.Find("Transparent/Diffuse");
                                renderer.material.color = new Color(color.r, color.g, color.b, 0.7f);

                                break;
                        }
                    }

                    Vector3 currentPos = pod.transform.position;
                    Transform facing = Player.main.camRoot.GetAimingTransform();
                    if (Input.GetKeyDown(KeyCode.Space))
                    {
                        speed = Vector3.zero;
                    }
                    else if (Input.GetKeyDown(KeyCode.W))
                    {
                        speed = 0.5f * new Vector3(facing.forward.x, 0, facing.forward.z);
                        speed.Normalize();
                        speed = speed * 0.1f;
                    }
                    else if (Input.GetKeyDown(KeyCode.A))
                    {
                        speed = 0.5f * new Vector3(-facing.right.x, 0, -facing.right.z);
                        speed.Normalize();
                        speed = speed * 0.1f;
                    }
                    else if (Input.GetKeyDown(KeyCode.S))
                    {
                        speed = 0.5f * new Vector3(-facing.forward.x, 0, -facing.forward.z);
                        speed.Normalize();
                        speed = speed * 0.1f;
                    }
                    else if (Input.GetKeyDown(KeyCode.D))
                    {
                        speed = 0.5f * new Vector3(facing.right.x, 0, facing.right.z);
                        speed.Normalize();
                        speed = speed * 0.1f;
                    }
                    else if (Input.GetKeyDown(KeyCode.C))
                    {
                        speed = 0.1f * new Vector3(0.0f, -1.0f, 0.0f);
                    }
                    else if (Input.GetKeyDown(KeyCode.X))
                    {
                        speed = 0.1f * new Vector3(0.0f, 1.0f, 0.0f);
                    }
                    
                    pod.transform.position += speed;
                    __instance.transform.position = pod.transform.position + new Vector3(0,2f,0);
                }

            }

            if (Input.GetKeyUp(UnleashedLifepodConfig.keyBinds["unleashedlifepod_modifier_key"]))
            {
                EscapePod pod = EscapePod.main;
                speed = Vector3.zero;
                foreach (var renderer in pod.GetComponentsInChildren<SkinnedMeshRenderer>())
                {
                    String gameObjectName = renderer.gameObject.name;
                    Color color = renderer.material.color;
                    switch (gameObjectName)
                    {
                        case "life_pod_interior_damaged1":
                            renderer.material.shader = EscapePod_Awake_Patch.originalLifePodShader;
                            renderer.material.color = new Color(color.r, color.g, color.b, 1.0f);

                            break;
                    }
                }
            }
            return true;
        }
    }
}
