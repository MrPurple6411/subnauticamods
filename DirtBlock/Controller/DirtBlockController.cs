﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace DirtBlock.Controllers {

    public class DirtBlockController : MonoBehaviour {

        Constructable construct;

        public GameObject parent;


        // Use this for initialization
        void Start()
        {
            construct = parent.GetComponent<Constructable>();
            
            if (construct == null)
            {
                Console.WriteLine("ERROR: Industrialization: MiningStationController: Parent constructable component not found");
            }
            
        }

    }

}
