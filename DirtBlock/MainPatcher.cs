﻿using System.Reflection;
using SMLHelper.V2.Handlers;
using Harmony;
using UnityEngine;
using System;
using SMLHelper.V2.Options;

namespace DirtBlock{
    public static class MainPatcher {

        public static AssetBundle assetBundle;

        public static void Patch()
        {

            var path = @"./QMods/DirtBlock/Assets/dirtblock";

            assetBundle = AssetBundle.LoadFromFile(path);

            if (assetBundle == null)
            {
                Console.WriteLine("ERROR: DirtBlock: Cannot locate asset bundle");
            }

            var dirtBlock = new DirtBlock();
            var randomBlock = new RandomBlock();

            dirtBlock.Patch();
            randomBlock.Patch();

            var harmony = HarmonyInstance.Create("com.oldark.subnautica.dirtblock.mod");
            harmony.PatchAll(Assembly.GetExecutingAssembly());

            OptionsPanelHandler.RegisterModOptions(new Options("Dirt Block"));
        }
    }

    public class Options : ModOptions {
        public Options(string name) : base(name)
        {
            try
            {
                ToggleChanged += OnToggleChanged;
            }
            catch(Exception e)
            {

            }
        }

        public override void BuildModOptions()
        {
            InitializeConfig();

            try
            {
                AddToggleOption("useVibrantColors", "Use Vibrant Colors", DirtBlockConfig.useVibrantColors);
            }
            catch(Exception e)
            {
                
            }
        }

        public void OnToggleChanged(object sender, ToggleChangedEventArgs args)
        {
            if (args.Id.Equals("useVibrantColors"))
            {
                DirtBlockConfig.useVibrantColors = args.Value;
                UpdatePlayerPrefs();
            }
        }

        void UpdatePlayerPrefs()
        {
            if (DirtBlockConfig.useVibrantColors)
            {
                PlayerPrefs.SetInt("DirtBlock_useVibrantColors", 1);
            }
            else
            {
                PlayerPrefs.SetInt("DirtBlock_useVibrantColors", 0);
            }

            PlayerPrefs.Save();
        }

        // Read previous option choices from player prefs to set defaults
        void InitializeConfig()
        {
            if (PlayerPrefs.HasKey("DirtBlock_useVibrantColors"))
            {
                if(PlayerPrefs.GetInt("DirtBlock_useVibrantColors") == 0)
                {
                    DirtBlockConfig.useVibrantColors = false;
                }
                else
                {
                    DirtBlockConfig.useVibrantColors = true;
                }
            }
        }
    }

    public class DirtBlockConfig : MonoBehaviour {
        public static bool useVibrantColors = false;
    }
}