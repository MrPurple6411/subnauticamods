﻿using Harmony;
using UnityEngine;

namespace QuickSave
{
    [HarmonyPatch(typeof(Player))]
    [HarmonyPatch("Update")]
    internal class Player_Update_Patch
    {
        [HarmonyPostfix]
        public static void Postfix()
        {
            string key;
            var path = @"./QMods/QuickSave/config.txt";
            System.IO.StreamReader file = new System.IO.StreamReader(path);
            key = file.ReadLine().Substring(14);
            // Keypresses put in any classes Update method that are called often will be listened for.
            if (Input.GetKeyDown(key))
            {
                IngameMenu.main.SaveGame();      // Runs the savegame function identically to the main menu
                IngameMenu.main.QuitSubscreen(); // Previous call can cause a 'ghost menu' to be brought up and invisible. This closes it.
            }
        }
    }
}
